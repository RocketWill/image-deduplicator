#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

import os
import click
import argparse
import shutil
import ntpath
import glob
import numpy as np
import cv2
import tqdm
import pprint
from imagededup.methods import PHash

def create_encoding_map(img_dir):
    files = [f for f in glob.glob(os.path.join(img_dir, "**/*"), recursive=True)]
    encode_map = {}
    for f in files:
        img_hash = phasher.encode_image(image_file=f)
        if img_hash:
            encode_map[f] = img_hash
    return encode_map



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Image Duplicator')
    parser.add_argument('--img-dir', default='./image_dir', help='Image directory')
    parser.add_argument('--max-dis', default=15, help='Max distance threshold', type=int)
    parser.add_argument('--duimg-dir', default='./duimage_dir', help='Path to store duplicated images.')
    
    args = parser.parse_args()
    image_dir = args.img_dir
    max_distance_threshold = int(args.max_dis)
    duimage_dir = args.duimg_dir
    os.makedirs(duimage_dir, exist_ok=True)

    phasher = PHash()
    encoding_map = create_encoding_map(image_dir)



    files_to_remove = phasher.find_duplicates_to_remove(encoding_map=encoding_map,
                      max_distance_threshold=max_distance_threshold)

    
    print("-"*50)
    print("Files to remove: ")
    pprint.pprint(files_to_remove)
    print("Number of all files {}.".format(len(encoding_map)))
    print("Number of pictures to be deleted: {}".format(len(files_to_remove)))
    print("-"*50)

    # if click.confirm('Do you want to move?', default=False):
    #     for file in tqdm.tqdm(files_to_remove):
    #         os.remove(os.path.join(image_dir, file))
    # else:
    if click.confirm('Do you want to move duplicated images to {}?'.format(duimage_dir), default=False):
        for file_ in tqdm.tqdm(files_to_remove):
            shutil.move(file_, os.path.join(duimage_dir, ntpath.basename(file_))) 
    else:
        print("Thank you for participating.")
    print("Done!")