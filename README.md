# Image Duplicator
## Requirement
```pip install imagededup```

## Usage
```python img_dedu.py --img-dir [/path/to/image/directory] --max-dis [max distance threshold] --duimg-dir [/path/to/store/duplicated/images]```
### parameters
1. `img-dir`: directory contains images
2. `max-dis`: 两幅图像间的汉明距离，在阈值之内的图像会被判定为重复，值为 0-64, default is `15`
3. `duimg-dir`: path to store duplicated images
